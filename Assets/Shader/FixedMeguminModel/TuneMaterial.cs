using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuneMaterial : MonoBehaviour
{
    public Material newMaterial;
    public Shader megumin;
    public MaterialInfo[] infos;

    void Start()
    {
        for (int i = 0; i < infos.Length; i++)
        {
            infos[i].UpdateMaterial(newMaterial,megumin);
        }
    }
}

[System.Serializable]
public struct MaterialInfo
{
    public GameObject appliedGO;
    public Texture2D albedo;

    [HideInInspector]
    public Material thisGOMaterial;

    public void UpdateMaterial(Material newMat, Shader shader) 
    {
        thisGOMaterial = new Material(shader);
        //thisGOMaterial.CopyPropertiesFromMaterial(newMat);
        thisGOMaterial.SetTexture("_MainTex", albedo);
        appliedGO.GetComponent<SkinnedMeshRenderer>().material = thisGOMaterial;
    }
}
