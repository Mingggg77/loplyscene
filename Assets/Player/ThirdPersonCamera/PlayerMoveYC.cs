using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveYC : MonoBehaviour
{
    private Rigidbody m_rigidbody;
    [System.Serializable]
    public class MoveSetting
    {
        public float mSpeed;//���ʳt��
        public float normalSpeed = 0; //�@��t��
        public float backSpeed = 1;//��h�t��
        public float horizontalSpeed = 1.5f;//��t��
        public float runSpeed = 5; //�]�B�t��

        public float jumpHight = 3; //���D�[�t��
        public float jumpForce = 70;//���D�O
        public bool grounded = true; //�ۦa�P�w
    }
    [System.Serializable]
    public class CameraSetting
    {
        public float mouseSensitivity = 2.0f; // �ƹ��F�ӫ�
        public float minAngle = -45.0f, maxAngle = 45.0f; //�����վ�
    }

    public MoveSetting move;
    public CameraSetting cameraSet;
    private Vector3 lastDirection;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }
    void Update()
    {
        //CameraAndAngle();
        MoveAndJump();
    }
    /// <summary>
    /// �۾����� �ƹ�����
    /// </summary>
    void CameraAndAngle()
    {
        float mouseY = Input.GetAxis("Mouse Y") * cameraSet.mouseSensitivity;//�������
        float mouseX = Input.GetAxis("Mouse X") * cameraSet.mouseSensitivity;

        Camera.main.transform.localRotation = Camera.main.transform.localRotation * Quaternion.Euler(-mouseY, 0, 0);
        Camera.main.transform.localRotation = ClampRotation(Camera.main.transform.localRotation, cameraSet.maxAngle, cameraSet.minAngle);

        transform.localRotation = transform.localRotation * Quaternion.Euler(0, mouseX, 0); //���⥪�k����
    }

    /// <summary>
    /// ���ʸ���D
    /// </summary>
    void MoveAndJump()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.LeftShift))
        { move.mSpeed = move.runSpeed; } //�]�B
        else if (Input.GetButton("Horizontal"))
        { move.mSpeed = move.horizontalSpeed; }//�
        else if (Input.GetKey(KeyCode.S))
        { move.mSpeed = move.backSpeed; }//��h
        else if (Input.GetButton("Vertical"))
        { move.mSpeed = move.normalSpeed; }
        else
        { move.mSpeed = 0f; }

        Vector3 newForward = Rotating(x, z); //player's transform

        if (newForward != Vector3.zero)
            transform.forward = newForward;

        //Vector3 morDir = x * transform.right + z * transform.forward;
        //m_rigidbody.MovePosition(morDir.normalized * move.mSpeed * Time.deltaTime + transform.position);//���ʦ�m

        m_rigidbody.MovePosition(transform.forward * move.mSpeed * Time.deltaTime + transform.position);//���ʦ�m

        if (Input.GetButtonDown("Jump"))  //jump
        {

            if (move.grounded == true)
            {
                m_rigidbody.velocity += new Vector3(0, move.jumpHight, 0); //�K�[�[�t��
                m_rigidbody.AddForce(Vector3.up * move.jumpForce); //������@�ӦV�W���O�A�O���j�p��Vector3.up*mJumpSpeed
                move.grounded = false;
            }
        }

        CastLine();
    }

    //void OnCollisionEnter(Collision collision)//�I���� �i�H�A��
    //{
    //    move.grounded = true;
    //}
    void CastLine() //�g�u����
    {
        Ray ray = new Ray(transform.position, Vector3.down);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, 1000f))
        {
            //Debug.Log($"{transform.position},{hitInfo.distance}"); 
            if (hitInfo.distance < 0.1)
            {
                move.grounded = true;
            }
            else
            {
                move.grounded = false;
            }
        }
    }

    Quaternion ClampRotation(Quaternion q, float maxAngle, float minAngle)
    {
        //�|���ƪ�xyzw�A���O���H�P�@�ӼơA�u���ܼҡA�����ܱ���
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1;

        /*���w�@�ӤשԱ���(X, Y, Z)�]�Y���O¶x�b�By�b�Mz�b����X�BY�BZ�ס^�A�h�������|���Ƭ�
        x = sin(Y/2)sin(Z/2)cos(X/2)+cos(Y/2)cos(Z/2)sin(X/2)
        y = sin(Y/2)cos(Z/2)cos(X/2)+cos(Y/2)sin(Z/2)sin(X/2)
        z = cos(Y/2)sin(Z/2)cos(X/2)-sin(Y/2)cos(Z/2)sin(X/2)
        w = cos(Y/2)cos(Z/2)cos(X/2)-sin(Y/2)sin(Z/2)sin(X/2)
         */

        //�o����ɤ���[�שԨ�x=2*Aan(q.x)]
        float angle = 2 * Mathf.Rad2Deg * Mathf.Atan(q.x);
        //����t��
        angle = Mathf.Clamp(angle, minAngle, maxAngle);
        //�ϱ��Xq���sx����
        q.x = Mathf.Tan(Mathf.Deg2Rad * (angle / 2));

        return q;
    }

    // Rotate the player to match correct orientation, according to camera and key pressed.
    Vector3 Rotating(float horizontal, float vertical)
    {
        Transform mainCamera = Camera.main.transform;
        // Get camera forward direction, without vertical component.
        Vector3 forward = mainCamera.TransformDirection(Vector3.forward);

        // Player is moving on ground, Y component of camera facing is not relevant.
        forward.y = 0.0f;
        forward = forward.normalized;

        // Calculate target direction based on camera forward and direction key.
        Vector3 right = new Vector3(forward.z, 0, -forward.x);
        Vector3 targetDirection = forward * vertical + right * horizontal;

        // Lerp current direction to calculated target direction.
        if ((horizontal != 0 || vertical != 0) && targetDirection != Vector3.zero)
        {
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
            Quaternion newRotation = Quaternion.Slerp(m_rigidbody.rotation, targetRotation, 0.002f);
            m_rigidbody.MoveRotation(newRotation);
            lastDirection = targetDirection;
        }

        return targetDirection;
    }
}

