using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootTest : MonoBehaviour
{
    public Camera cam;
    public GameObject projectile;
    public Transform FirePoint;//���k�⪺�y��
    public float projectileSpeed = 30.0f;

    private Vector3 destination;
    

    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            ShootProjection();
        }
    }
    void ShootProjection ()
    {
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;
        if (Physics.Raycast (ray,out hit ))
        {
            destination = hit.point;
        }
        else
        {
            destination = ray.GetPoint(1000);
        }
        InstantiateProjection(FirePoint);
    }
    void InstantiateProjection(Transform firePoint)
    {
        var projectileobj = Instantiate(projectile, firePoint.position, Quaternion.identity) as GameObject;
        projectileobj.GetComponent<Rigidbody>().velocity = (destination - firePoint.position).normalized* projectileSpeed;
    }
}
