using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private Rigidbody m_rigidbody;

    [System.Serializable]
    public class MoveSetting
    {
        public float mSpeed;//���ʳt��
        public float normalSpeed = 1.5f; //�@��t��
        public float backSpeed = 1;//��h�t��
        public float horizontalSpeed = 1.2f;//��t��
        public float runSpeed = 4; //�]�B�t��
        [HideInInspector]
        public float staticSpeed = 0f;

        public float jumpHight = 3; //���D�[�t��
        public float jumpForce = 70;//���D�O
        public float groundClearance;
        public bool grounded = true; //�ۦa�P�w
        
        #region debug
        public bool enforceControl = false; //
        #endregion
    }
    [System.Serializable]
    public class CameraSetting
    {
        public float mouseSensitivity = 2.0f; // �ƹ��F�ӫ�
        public float minAngle = -45.0f, maxAngle = 45.0f; //�����վ�
    }

    public MoveSetting moveSet;
    public CameraSetting cameraSet;
    private Vector3 lastDirection;
    public float rotatingSmooth = 0.02f;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_rigidbody.freezeRotation = true;  //�T��I������
    }
    void Update()
    {
        //CameraAndAngle();
        MoveAndJump();
    }
    /// <summary>
    /// �۾����� �ƹ�����
    /// </summary>
    void CameraAndAngle()
    {
        float mouseY = Input.GetAxis("Mouse Y") * cameraSet.mouseSensitivity;//�������
        float mouseX = Input.GetAxis("Mouse X") * cameraSet.mouseSensitivity;

        Camera.main.transform.localRotation = Camera.main.transform.localRotation * Quaternion.Euler(-mouseY, 0, 0);
        Camera.main.transform.localRotation = ClampRotation(Camera.main.transform.localRotation, cameraSet.maxAngle, cameraSet.minAngle);

        transform.localRotation = transform.localRotation * Quaternion.Euler(0, mouseX, 0); //���⥪�k����
    }
    /// <summary>
    /// ���ʸ���D
    /// </summary>
    void MoveAndJump()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.LeftShift))
        { moveSet.mSpeed = moveSet.runSpeed; } //�]�B
        else if (Input.GetButton("Horizontal"))
        { moveSet.mSpeed = moveSet.horizontalSpeed; }//�
        else if (Input.GetKey(KeyCode.S))
        { moveSet.mSpeed = moveSet.backSpeed; }//��h
        else if (Input.GetButton("Vertical"))
        { moveSet.mSpeed = moveSet.normalSpeed; }
        else
        { moveSet.mSpeed = moveSet.staticSpeed; }

        //Vector3 newForward = Rotating(x, z); //player's transform
        Rotating(x, z);
        //if (newForward != Vector3.zero)
        //    transform.forward = newForward;

        if(moveSet.enforceControl) 
            m_rigidbody.MovePosition(transform.forward * moveSet.mSpeed * Time.deltaTime + transform.position);//���ʦ�m

        if (Input.GetButtonDown("Jump"))  //jump
        {
            if (moveSet.grounded == true)
            {
                m_rigidbody.velocity += new Vector3(0, moveSet.jumpHight, 0); //�K�[�[�t��
                m_rigidbody.AddForce(Vector3.up * moveSet.jumpForce); //������@�ӦV�W���O�A�O���j�p��Vector3.up*mJumpSpeed
                moveSet.grounded = false;
            }
        }
        CastLine();
    }
    //void OnCollisionEnter(Collision collision)//�I���� �i�H�A��
    //{
    //    move.grounded = true;
    //}
    void CastLine() //�g�u���� �ۦa�P�w
    {
        Ray ray = new Ray(transform.position + Vector3.up * 0.5f, Vector3.down);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, 1000f))
        {
            moveSet.groundClearance = hitInfo.distance;
            //Debug.Log($"{transform.position},{hitInfo.distance}"); 
            if (hitInfo.distance < 0.6)
            {
                moveSet.grounded = true;
            }
            else
            {
                moveSet.grounded = false;
            }
        }
    }

    Quaternion ClampRotation(Quaternion q, float maxAngle, float minAngle)
    {
        //�|���ƪ�xyzw�A���O���H�P�@�ӼơA�u���ܼҡA�����ܱ���
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1;

        /*���w�@�ӤשԱ���(X, Y, Z)�]�Y���O¶x�b�By�b�Mz�b����X�BY�BZ�ס^�A�h�������|���Ƭ�
        x = sin(Y/2)sin(Z/2)cos(X/2)+cos(Y/2)cos(Z/2)sin(X/2)
        y = sin(Y/2)cos(Z/2)cos(X/2)+cos(Y/2)sin(Z/2)sin(X/2)
        z = cos(Y/2)sin(Z/2)cos(X/2)-sin(Y/2)cos(Z/2)sin(X/2)
        w = cos(Y/2)cos(Z/2)cos(X/2)-sin(Y/2)sin(Z/2)sin(X/2)
         */

        //�o����ɤ���[�שԨ�x=2*Aan(q.x)]
        float angle = 2 * Mathf.Rad2Deg * Mathf.Atan(q.x);
        //����t��
        angle = Mathf.Clamp(angle, minAngle, maxAngle);
        //�ϱ��Xq���sx����
        q.x = Mathf.Tan(Mathf.Deg2Rad * (angle / 2));

        return q;
    }

    // Rotate the player to match correct orientation, according to camera and key pressed.
    Vector3 Rotating(float horizontal, float vertical)
    {
        Transform mainCamera = Camera.main.transform;
        // Get camera forward direction, without vertical component.
        Vector3 forward = mainCamera.TransformDirection(Vector3.forward);

        // Player is moving on ground, Y component of camera facing is not relevant.
        forward.y = 0.0f;
        forward = forward.normalized;

        // Calculate target direction based on camera forward and direction key.
        Vector3 right = new Vector3(forward.z, 0, -forward.x);
        //Vector3 targetDirection = forward * vertical + right * horizontal;
        Vector3 targetDirection = forward;

        // Lerp current direction to calculated target direction.
        if ((horizontal != 0 || vertical != 0) && targetDirection != Vector3.zero)
        {
            Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
            Quaternion newRotation = Quaternion.Slerp(transform.rotation, targetRotation, rotatingSmooth);
            transform.rotation = newRotation;
            lastDirection = targetDirection;
        }

        if (!(Mathf.Abs(horizontal) > 0.9 || Mathf.Abs(vertical) > 0.9))
        {
            if (lastDirection != Vector3.zero)
            {
                lastDirection.y = 0;
                Quaternion targetRotation = Quaternion.LookRotation(lastDirection);
                Quaternion newRotation = Quaternion.Slerp(transform.rotation, targetRotation, rotatingSmooth);
                transform.rotation = newRotation;
            }
        }

        return targetDirection;
    }
}

