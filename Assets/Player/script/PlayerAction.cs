using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : MonoBehaviour
{
    public float runJumpAcc;  
    public float walkJumpAcc;

    private Animator anim;
    private PlayerMove move;
    private Rigidbody rigidbody;
    private Vector3 inertialVel;
    private float acce;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        move = GetComponent<PlayerMove>();
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        //NormalMode2();
        InAttackMode();
        //if (Input.GetMouseButton(0)) //�i�J�԰� ����i�H����� �p�J�� �令��Ʊ���
        //{
        //    AttackMode();
        //}
        Unground();
    }
    void NormalMode()
    {

        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                anim.SetBool("run", true);
            }
            else
            {
                anim.SetBool("run", false);
                anim.SetBool("walk", true);
            }
            if ((Input.GetKey(KeyCode.S)))
            {
                anim.SetBool("back", true);
            }
            else
            {
                anim.SetBool("back", false);
            }
        }
        else
        {
            anim.SetBool("walk", false);
            anim.SetBool("back", false);
            anim.SetBool("run", false);
        }

        if (Input.GetButton("Jump"))
        {
            anim.SetBool("jump", true);
        }
        else
        {
            anim.SetBool("jump", false);
        }

    }
    void AttackMode()
    {
        if (Input.GetMouseButton(0)) // �]�w�J�Ǳ���  ������R���o��
        {
            anim.SetBool("alert", true);

        }
        else // �令if OR ����԰��ɫ����}
        {
            anim.SetBool("alert", false);
        }
        NormalMode2();
    }
    void InAttackMode()
    {
        if (Input.GetButton("Horizontal") || Input.GetButton("Vertical"))
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");
            anim.SetFloat("hori", x);
            anim.SetFloat("vert", z);
            if (Input.GetKey(KeyCode.LeftShift)|| Input.GetKey(KeyCode.C))
            {
                anim.SetBool("run", true);
            }
            else
            {
                anim.SetBool("walk", true);
            }
        }
        else
        {
            anim.SetBool("walk", false);
            anim.SetBool("run", false);
        }

        if (Input.GetButton("Jump"))
        {
            anim.SetBool("jump", true);

            inertialVel = rigidbody.velocity;
            if (anim.GetBool("run"))
                acce = runJumpAcc;
            else
                acce = walkJumpAcc;
        }
    }

    private void FixedUpdate()
    {
        if (inertialVel != Vector3.zero)
        {
            //jump displacement = vt+1/2at^2, where v= inertial velocity, a= assumed force
            transform.position += 2*inertialVel * Time.fixedDeltaTime
                + Vector3.Normalize(inertialVel) * 0.5f * acce * Time.fixedDeltaTime * Time.fixedDeltaTime;
        }

    }
    private void LateUpdate()
    {
        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Standing_Jump")
            || !anim.GetCurrentAnimatorStateInfo(0).IsName("Standing_Jump_Running 0"))
        {
            anim.SetBool("jump", false);
            inertialVel = Vector3.zero;
        }
    }
    void NormalMode2()
    {
        if (Input.GetKey(KeyCode.W))
        {
            anim.SetBool("walk", true);
        }
        else
        {
            anim.SetBool("walk", false);
        }
        if (Input.GetKey(KeyCode.A))
        {
            anim.SetBool("left", true);
        }
        else
        {
            anim.SetBool("left", false);
        }
        if (Input.GetKey(KeyCode.S))
        {
            anim.SetBool("back", true);
        }
        else
        {
            anim.SetBool("back", false);
        }
        if (Input.GetKey(KeyCode.D))
        {
            anim.SetBool("right", true);
        }
        else
        {
            anim.SetBool("right", false);
        }
        if (Input.GetButton("Jump"))
        {
            anim.SetBool("jump", true);
        }
        else
        {
            anim.SetBool("jump", false);
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetBool("run", true);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            anim.SetBool("run", false);
        }
    }

    void Unground()
    {
        float airDistance = move.moveSet.groundClearance;
        //Debug.Log("airDistance" + airDistance);
        anim.SetFloat("unGround", airDistance);
        //if (airDistance >= 2.7f && <=1.0f) //�Z�����ʵe�b�վ�  �γ~ �ʵe�Ȱ�
        //{
        //    anim.speed = 0;
        //}
        //else
        //{
        //    anim.speed = 1;
        //}
    }

}

