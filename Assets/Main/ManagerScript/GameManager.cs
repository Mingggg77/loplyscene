using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public ChangeSceneManager CSM;

    void Awake()
    {
        if (instance != null)
        {
            Debug.LogFormat(gameObject,
                    "Multiple instances of {0} is not allow", GetType().Name);
            DestroyImmediate(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        instance = this;
        ChangeScene("Start");
    }

    public void ChangeScene(string sceneName) => CSM.ChangeScene(sceneName);
    public void quitGame() => Application.Quit();
}
