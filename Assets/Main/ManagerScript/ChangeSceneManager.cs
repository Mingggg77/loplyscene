using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class ChangeSceneManager : Singleton<ChangeSceneManager>
{
    public SceneInfo[] sceneList;

    ChangeSceneManager()
    {
    }

    public void ChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);

        for (int i = 0; i < sceneList.Length; i++)
        {
            if (sceneName == sceneList[i].sceneName)
                sceneList[i].UI.SetActive(true);
            else
                sceneList[i].UI.SetActive(false);
        }
    }
}

[System.Serializable]
public struct SceneInfo
{
    public string sceneName;
    public GameObject UI;
}
