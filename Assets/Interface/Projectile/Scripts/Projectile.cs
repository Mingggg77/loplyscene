using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    #region Public Properties

    public float speed=1f;         // Projectile speed
    public float radiusAOE=1f;     // Projectile effect radius
    public float dmgAOE=1f;        // Projectile damage
    public float lifeTime = 3f;    // Projectile lifetime
    public int maxPT=1;            // max number of penetration
    public bool deformSurf=false;  // enable deform surface or not

    #endregion

    #region Hidden Properties

    [HideInInspector]
    public bool isLife;           // Projectile is observed or not
    [HideInInspector]
    public int numHit;            // record how many object hitted by this Projectile

    #endregion

    void Awake()
    {
        Init();
    }

    void FixedUpdate()
    {
        UpdatePosition();
        CheckStatue();
    }

    protected void UpdatePosition()
    {
        transform.position += transform.forward * speed * Time.deltaTime;
    }
    protected void CheckStatue()
    {
        if (maxPT <= numHit)
        {
            Debug.Log($"captured by receiever {numHit}");
            isLife = false;
            DestroyObject(this.gameObject);
        }
    }
    protected void Init()
    {
        ProjectilePool.activeProjectiles.Add(this);

        DestroyObject(this.gameObject, lifeTime);
        isLife = true;
        numHit = 0;
    }

    public void Fire(Vector3 emitPos, Vector3 targetPos)
    {
        transform.position = emitPos;
        transform.forward = targetPos - emitPos;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position,radiusAOE);
    }

    void OnDestroy()
    {
        ProjectilePool.activeProjectiles.Remove(this);
    }
}
