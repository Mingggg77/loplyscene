using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReceiver : MonoBehaviour
{
    public ColliderSphere[] colliders;  // use sphere to simplified body
    float boundingRadius;               // radius of bounding sphere to cover all colliderSphere
    Vector3 _boundingCenter;            // center of bounding sphere in local
    Vector3 boundingCenter { set {} get => _boundingCenter + transform.position; } // center of bounding sphere in global
    Dictionary<Projectile, int> hitList = new Dictionary<Projectile, int>();       // list of projectiles to avoid mutiple collision
    Projectile receiveP;                // projectile hits colliderSphere
    ColliderSphere getDmgS;             // colliderSphere receives projectile

    [HideInInspector]
    public float totalDmg;              // accumulation of damage

    void Awake()
    {
        totalDmg = 0f;
        boundingRadius = GetBoundingSphere(colliders, out _boundingCenter);
    }

    void FixedUpdate()
    {
        if (DetectCollision(colliders, hitList, transform, boundingCenter, boundingRadius, out receiveP, out getDmgS))
        {
            totalDmg += GetDmg(receiveP, getDmgS, hitList);
            Debug.Log($"Collider \"{getDmgS.name}\" receieves projectile, totalDmg= {totalDmg}");
        }
    }

    static bool DetectCollision(ColliderSphere[] colliders, Dictionary<Projectile, int> hitList, Transform transform, Vector3 boundingCenter, float boundingRadius, out Projectile receiveP, out ColliderSphere getDmgS)
    {
        receiveP = null;
        getDmgS = default;

        for (int i = 0; i < ProjectilePool.activeProjectiles.Count; i++)
        {
            Projectile b = ProjectilePool.activeProjectiles[i];

            if (b.isLife && !hitList.ContainsKey(b))
            {
                Vector3 b_pos = b.transform.position;
                float b_rad = b.radiusAOE;

                bool inBounding = DetectOverlap(b_pos, b_rad, boundingCenter, boundingRadius);

                if (inBounding)
                {
                    for (int j = 0; j < colliders.Length; j++)
                    {
                        Vector3 colliderCenter = colliders[j].center + transform.position;
                        bool isHit = DetectOverlap(b_pos, b_rad, colliderCenter, colliders[j].radius);

                        if (isHit)
                        {
                            getDmgS = colliders[j];
                            receiveP = b;
                            return true;
                        }
                    }
                }
            }
            else
            {
                if (hitList.ContainsKey(b))
                    hitList.Remove(b);
            }
        }

        return false;
    }

    static float GetDmg(Projectile receiveP, ColliderSphere getDmgS, Dictionary<Projectile, int> hitList)
    {
        bool isGetDmg = false;
        float receiveDmg = 0f;

        if (hitList.ContainsKey(receiveP)) // this projectile already casued dmg 
        {
            receiveDmg = 0f;
            return receiveDmg;
        }
        else // this projectile first casues dmg 
        {
            hitList.Add(receiveP, 1);
            isGetDmg = true;
        }

        if (isGetDmg)
        {
            receiveDmg += receiveP.dmgAOE * getDmgS.ratio;
            receiveP.numHit++;
        }

        return receiveDmg;
    }

    static float GetBoundingSphere(ColliderSphere[] colliders, out Vector3 avgCenter)
    {
        float maxRadius = 0f;
        avgCenter = Vector3.zero;

        for (int i = 0; i < colliders.Length; i++)
        {
            avgCenter += colliders[i].center;
        }

        avgCenter /= colliders.Length;

        for (int i = 0; i < colliders.Length; i++)
        {
            float r = (colliders[i].center - avgCenter).magnitude + colliders[i].radius;
            if (r > maxRadius) maxRadius = r;
        }
        maxRadius *= 1.2f;

        return maxRadius;
    }

    static bool DetectOverlap(Vector3 center1, float radius1, Vector3 center2, float radius2)
    {
        float distance = (center2 - center1).magnitude;

        if (distance < radius1 + radius2)
            return true;

        return false;
    }

    void OnDrawGizmos()
    {
        //each DamageReceiver
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].color.a = 1f;
            Gizmos.color = colliders[i].color;
            Gizmos.DrawWireSphere(colliders[i].center + transform.position, colliders[i].radius);
        }

        //outer shell
        boundingRadius = GetBoundingSphere(colliders, out _boundingCenter);
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(boundingCenter, boundingRadius);
    }

    [System.Serializable]
    public struct ColliderSphere
    {
        public string name;
        public float radius;
        public float ratio;
        public Vector3 center;
        public Color color;
    }
}
