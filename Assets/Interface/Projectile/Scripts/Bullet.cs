using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Projectile
{

     void Awake()
    {
        base.Init();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        base.UpdatePosition();
        base.CheckStatue();
    }
}
