using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    #region Public Properties

    ProjectileEmitter PE;
    public float fMoveMul;
    public float fControllerSensitivity;
    public LayerMask hitLayers;

    #endregion
    Transform cT;

    void Start()
    {
        cT = Camera.main.transform;
        PE = GetComponent<ProjectileEmitter>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray r = new Ray(cT.position, cT.forward);
            RaycastHit rh;
            Vector3 vTarget = cT.position + cT.forward*100.0f;
            if (Physics.Raycast(r, out rh, 100.0f, hitLayers))
            {
                Vector3 vDir = rh.point - cT.position;
                vTarget = rh.point;
                float fD = vDir.magnitude;
                if(fD < 10.0f)
                {
                    vTarget = cT.position + cT.forward * 10.0f;
                } 
            }

            PE.EmitProjectile(vTarget);
        }

        float fMX = Input.GetAxis("Mouse X")* fControllerSensitivity;
        float fMY = Input.GetAxis("Mouse Y")* fControllerSensitivity;
        transform.Rotate(0.0f, fMX, 0.0f);
        cT.Rotate(-fMY, 0.0f, 0.0f);
       
        float fH = Input.GetAxis("Horizontal");
        float fV = Input.GetAxis("Vertical");

        Vector3 vMove = transform.forward * fV;
        vMove += transform.right * fH;
        vMove = vMove*Time.deltaTime* fMoveMul;
        transform.position += vMove;
    }
}
