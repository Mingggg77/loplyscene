using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ProjectileEmitter : MonoBehaviour
{
    public GameObject projectile;
    public GameObject emitPoint;

    private void Awake()
    {
        if (projectile.GetComponent<Projectile>() is null)
            Debug.LogError($"GameObject {projectile.name} lacks script based on Projectile.cs");
    }

    public void EmitProjectile(Vector3 orient)
    {
        GameObject gn = Instantiate(projectile, emitPoint.transform.position, Quaternion.identity);
        gn.GetComponent<Projectile>().Fire(emitPoint.transform.position, orient);
    }
}
